import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Container, Row, Col, Form, FormControl, Button } from 'react-bootstrap';
import AutoComplete from '../components/AutoComplete.jsx';
import Header from "../img/header.jpg";
import Forecasts from '../components/Forecasts.jsx';
import '../pages/css/WeatherPage.css';
import { currentConditionsUrl, apiKey, readFromServer, forecastsUrl } from '../server/Helpers.js';
import CurrentWeather from '../components/CurrentWeather.jsx';



function WeatherPage() {
    const [city, setCity] = useState({
        LocalizedName: "Tel Aviv",
        Key: "215854",
    });
    const [cityWeather, setCityWeather] = useState({});
    const [forecastsData, setForecastsData] = useState({});
    useEffect(() => {
        localStorage.getItem('redirected') ? pageRedirected() : citySelected(city);
    }, []);

    return (
        <div>

            <Container fluid>
                <Row>
                    <Col>
                        <AutoComplete city={city} onCitySelected={(city) => citySelected(city)} />
                        {cityWeather.Temperature ? <CurrentWeather city={city} cityWeather={cityWeather}></CurrentWeather> : ''}
                        {forecastsData.DailyForecasts ? <Forecasts forecastsData={forecastsData} ></Forecasts> : ''}
                    </Col>
                </Row>
            </Container>
        </div>

    );

    function citySelected(selectedCity) {
        setCity(selectedCity);
        getCurrentConditions(selectedCity);
    }

    function getCurrentConditions(selectedCity) {
        const time = Math.round((new Date()).getTime() / 1000);


        readFromServer(currentConditionsUrl + selectedCity.Key + '?apikey=' + apiKey + '&details=true',
            "GET").then((response) => {
                setCityWeather(response[0]);
            });
        getForecast(selectedCity.Key);
    }

    function getForecast(cityKey) {
        readFromServer(forecastsUrl + cityKey + '?apikey=' + apiKey + '&details=true',
            "GET").then((response) => {
                setForecastsData(response);
            });

    }

    function pageRedirected() {
        var redirected = JSON.parse(localStorage.getItem('redirected'));
        citySelected(redirected)
    }

}

export default WeatherPage;


