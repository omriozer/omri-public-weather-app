import React, { useState } from 'react';
import FavoritesComponent from '../components/FavoriteComponent';

import { currentConditionsUrl, apiKey, readFromServer, forecastsUrl } from '../server/Helpers.js';

import '../pages/css/FavoritePage.css'
import { Container, Row, Col } from 'react-bootstrap';



function FavoritesPage() {
    return (
        <Container fluid>
            <Row>
                {getAllFavorites()}
            </Row>
        </Container>


    );
}

export default FavoritesPage;

function getAllFavorites() {
    var favoritesList = [],
        keys = Object.keys(localStorage),
        i = keys.length;
    let num = 0;

    while (i--) {
        let storageItem = JSON.parse(localStorage.getItem(keys[i]));
        if (storageItem.favorite) {
            num++;
            let favoriteCity = <FavoritesComponent id={num} storedCity={storageItem}></FavoritesComponent>;
            favoritesList.push(favoriteCity);
        }

    }



    if (favoritesList.length > 0) {
        return favoritesList;
    } else {
        return (
            <Container fluid>
                <Row>
                    <Col>
                        <div className="mt-5">You did not save any favorites... yet...</div>
                    </Col>
                </Row>
            </Container>

        );
    }
}


