import { Container, Row, Col } from "react-bootstrap";
import React, { useState } from 'react';

const error = localStorage.getItem('error');
export const displayError = () => {
    if (error) {
        return (
            <Container fluid className="errorBG">
                <Row>
                    <Col>
                        <div className="errorContaier">
                            <div className="error">
                                {error}
                            </div>
                        </div>

                    </Col>
                </Row>
            </Container>

        );
    }
}

export const readFromServer = (url, command) => {
    let headers = {
        Accept: "application/json, text/plain, /",
    };

    let params = {
        method: command,
        mode: "cors",
        cache: "no-cache",
        headers: headers,
    };

    return fetch(url, params).then((response) =>
        response
            .json()
            .then((json) => {
                return json;
            })
            .catch((error) => {
                localStorage.setItem('error', error)
                return null;
            })
    );
}

export const forecastsUrl = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/";
export const currentConditionsUrl = "http://dataservice.accuweather.com/currentconditions/v1/";
export const autoCompleteUrl = "http://dataservice.accuweather.com/locations/v1/cities/autocomplete";
export const apiKey = "zwVkmDleAHAngCYTtWfngMBX3LbKtmpc";

