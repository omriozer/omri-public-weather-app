import React, { Component } from 'react';
import logo from './logo.svg';
import WeatherNavbar from './components/WeatherNavbar.jsx';
import WeatherPage from './pages/WeatherPage.jsx';
import FavoritesPage from './pages/FavoritesPage';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { displayError } from './server/Helpers';


function App() {

  return (
    <div className="App">
      <WeatherNavbar></WeatherNavbar>
      {/* {displayError()} */}
      <Router>
        <Switch>
          <Route path="/weather/" component={WeatherPage} />
          <Route path="/favorites/" component={FavoritesPage} />
          {/* <Route component={PageNotFound} /> */}
        </Switch>
      </Router>
    </div>
  );
}

export default App;

