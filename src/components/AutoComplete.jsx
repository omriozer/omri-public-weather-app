import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave, faEraser } from '@fortawesome/free-solid-svg-icons';

import { autoCompleteUrl, apiKey, readFromServer } from '../server/Helpers.js';

import { Row, Container } from 'react-bootstrap';



function AutoComplete(props) {
    let [value, setValue] = useState('');
    const [optionOpen, setOptionOpen] = useState(false);
    const [citys, setCitys] = useState([]);

    const faSaveIcon = <FontAwesomeIcon className="ml-2" icon={faSave} />
    const faEraserIcon = <FontAwesomeIcon className="ml-2" icon={faEraser} />

    return (
        <Container fluid className="inputContainer">
            <Row>
                <input
                    className="searchInput"
                    placeholder={props.city.LocalizedName}
                    value={value}
                    autoComplete="off"
                    type="text"
                    id="fname"
                    name="fname"
                    onBlur={() => setTimeout(() => {
                        setOptionOpen(false)
                    }, 300)}
                    onChange={(event) => inputChanged(event.target.value)}
                />
            </Row>
            <Row>
                <div className={optionOpen ? "searchInput-container" : "close"}>
                    <div>
                        {makeCitysList()}
                    </div>
                </div>
            </Row>
        </Container>

    );


    function inputChanged(input) {
        setValue(input)
        input.length > 0 ? setOptionOpen(true) : setOptionOpen(false)
        if (input.length > 2) {
            readFromServer(autoCompleteUrl + "?apikey=" + apiKey + "&q=" + input,
                "GET").then((response) => {
                    setCitys(response);
                    makeCitysList();
                });
        }
    }


    function makeCitysList() {

        let cityItemList = []
        for (let index = 0; index < citys.length; index++) {
            let city = citys[index];
            let cityName = city.LocalizedName;
            city.favorite = localStorage.getItem(cityName) ? false : true;
            let cityItem =
                <div className="ListCitysItem">
                    <div onClick={() => handleFavoriteStatus(city)} key={`${cityName}_star`} className="favoriteIconDiv">
                        {city.favorite ? faSaveIcon : faEraserIcon}
                    </div>
                    <div className="cityNameDiv">
                        <input key={`${cityName}_opt`} type="button" value={city.LocalizedName} className="cityNameInput" onClick={() => handleCityChoose(city)} />

                    </div>
                </div>

            cityItemList.push(cityItem);
        }
        return cityItemList;
    }


    function handleFavoriteStatus(city) {
        city.favorite ? localStorage.setItem(city.LocalizedName, JSON.stringify(city)) : localStorage.removeItem(city.LocalizedName)
        handleCityChoose(city);
    }

    function handleCityChoose(city) {
        setValue(city.LocalizedName);
        setOptionOpen(false);
        props.onCitySelected(city);
    }

}

export default AutoComplete;