import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';

function CurrentWeather(props) {
    const cityWeather = props.cityWeather;
    const city = props.city;
    const weatherIconNum = cityWeather.WeatherIcon < 10 ? '0' + cityWeather.WeatherIcon : cityWeather.WeatherIcon;
    // if () {

    return (
        <Container fluid>
            <Row>
                <Col>
                    <div className="currentcityWeatherNameDisplay">current weather in:<br /> {props.city.LocalizedName}</div>
                    <div className="CurrentWeatherBoxDiv weatherBox">
                        <div className="boldText tempDiv">{cityWeather.Temperature.Metric.Value}  <span>{'\u00b0'}</span> {cityWeather.Temperature.Metric.Unit}</div>
                        <div className="weatherIconDiv"><img src={`https://developer.accuweather.com/sites/default/files/${weatherIconNum}-s.png`} alt="" /></div>
                        <div className="boldText WeatherTextDiv">{cityWeather.WeatherText}</div>
                        <div><span className="boldText dataDisplayDiv">UV Index:</span> {cityWeather.UVIndex + ' ' + cityWeather.UVIndexText}</div>
                        <div><span className="boldText dataDisplayDiv">Wind:</span> {cityWeather.Wind.Speed.Metric.Value + ' ' + cityWeather.Wind.Speed.Metric.Unit}</div>
                        <div><span className="boldText dataDisplayDiv">Wind Gusts:</span> {cityWeather.WindGust.Speed.Metric.Value + ' ' + cityWeather.Wind.Speed.Metric.Unit}</div>
                        <div><span className="boldText dataDisplayDiv">Humidity:</span> {cityWeather.RelativeHumidity + '%'}</div>
                        <div><span className="boldText dataDisplayDiv">Dew Point:</span> {cityWeather.DewPoint.Metric.Value} <span>{'\u00b0'}</span> {cityWeather.DewPoint.Metric.Unit}</div>
                        <div><span className="boldText dataDisplayDiv">Cloud Cover:</span> {cityWeather.Ceiling.Metric.UnitType + '%'}</div>
                        <div><span className="boldText dataDisplayDiv">Cloud Visibility:</span> {cityWeather.Ceiling.Metric.Value + cityWeather.Ceiling.Metric.Unit}</div>
                        <div><span className="boldText dataDisplayDiv">Cloud Ceiling:</span> {cityWeather.Visibility.Metric.Value + ' ' + cityWeather.Visibility.Metric.Unit}</div>
                        <div><span className="boldText dataDisplayDiv">Indoor Humidity:</span> {cityWeather.IndoorRelativeHumidity + '%'}</div>
                    </div>
                </Col>
            </Row>
        </Container>
    );

}

export default CurrentWeather;