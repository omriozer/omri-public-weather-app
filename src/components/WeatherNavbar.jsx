import React, { useState } from 'react';
import { Navbar, Nav, Container, Row, Col } from 'react-bootstrap';

function WheatherNavbar(props) {
    return (
        <div>
            <Navbar bg="light" expand="lg">
                <Navbar.Brand href="weather">Herolo-wheather</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link href="weather">Weather details</Nav.Link>
                        <Nav.Link href="favorites">Favorites</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </div>
    );
}



export default WheatherNavbar;
