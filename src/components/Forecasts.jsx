import React, { useState } from 'react';
import { Row, Container, Col } from 'react-bootstrap';

import WeekWeather from '../components/WeekWeather';


function Forecasts(props) {
    const forecastsData = props.forecastsData;
    return (
        <Container fluid>
            <Row>
                {creatForecastsComponentsList(forecastsData)}
            </Row>
        </Container>

    );
}

export default Forecasts;


function creatForecastsComponentsList(forecastsData) {
    var dailyForecastsComponents = [];
    for (var i = 0; i < forecastsData.DailyForecasts.length; i++) {
        dailyForecastsComponents.push(
            <Col key={'forecastsData_col_' + i}>
                <WeekWeather key={'forecastsData_' + i} forecastsData={forecastsData.DailyForecasts[i]}></WeekWeather>
            </Col>);
    }
    return dailyForecastsComponents;
}