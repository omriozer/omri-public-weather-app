import React, { useState, useEffect } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEraser } from '@fortawesome/free-solid-svg-icons';
import { readFromServer, currentConditionsUrl, apiKey } from '../server/Helpers';

const faEraserIcon = <FontAwesomeIcon className="ml-2" icon={faEraser} />


function FavoritesComponent(props) {
    const city = props.storedCity;
    const [favoriteCurrentWeather, setfavoriteCurrentWeather] = useState({});
    const weatherIconNum = favoriteCurrentWeather.WeatherIcon < 10 ? '0' + favoriteCurrentWeather.WeatherIcon : favoriteCurrentWeather.WeatherIcon;
    useEffect(() => {
        readFromServer(currentConditionsUrl + city.Key + '?apikey=' + apiKey + '&details=true',
            "GET").then((response) => {
                setfavoriteCurrentWeather(response[0])
            });

    }, []);


    return (
        <Container fluid>
            <Row>
                <div className="eraserIcon" onClick={() => removeFavorite(city.LocalizedName)} className="eraserIcon">{faEraserIcon}</div>
                <div onClick={() => redirectToCityWeather(city)} className="favoriteItem">{props.id}. {city.LocalizedName}</div>
                <div className="favoriteWeatherIconDiv"><img src={`https://developer.accuweather.com/sites/default/files/${weatherIconNum}-s.png`} alt="weatherIcon" /></div>
            </Row>
        </Container>
    );
}

function removeFavorite(cityName) {
    localStorage.removeItem(cityName);
    window.location.reload(false);

}

export default FavoritesComponent;


function redirectToCityWeather(city) {
    localStorage.setItem('redirected', JSON.stringify(city));
    window.location.href = 'weather';
}